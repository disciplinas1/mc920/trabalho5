%-----------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%-----------------------------------------------

\documentclass[a4paper, 11pt]{article}

\usepackage{geometry}
\geometry{
    a4paper,
    top    = 1.5cm,
    bottom = 1.5cm,
    left   = 2.0cm,
    right  = 2.0cm
}

% \usepackage[utf8]{inputenc}
% \usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{mathtools}
\usepackage[portuguese]{babel}
\usepackage[version=3]{mhchem}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{indentfirst}
\usepackage{titlesec}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{etoolbox}
\usepackage{xcolor}
\usepackage{pdfpages}
\usepackage{listings}
\usepackage{subcaption}

%--- REFS --------------------------------

\usepackage{hyperref}
\usepackage[nameinlink,capitalize]{cleveref}

\hypersetup{
    pdftitle={Trabalho 5 -- RA220063},
    pdfauthor={Leonardo de Sousa Rodrigues},
    colorlinks=true,
    linkcolor=blue,
    citecolor=red
}

%--- CODE LISTING ----------------------

\usepackage{listings}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}


% Enlarge spacing in align environment
% \addtolength{\jot}{8pt}


%--- EQREF ----------------------------------------

%equations ref brackets
\makeatletter
\let\oldtheequation\theequation
\renewcommand\tagform@[1]{\maketag@@@{\ignorespaces#1\unskip\@@italiccorr}}
\renewcommand\theequation{(\oldtheequation)}
\makeatother


% align elements in matrix
\makeatletter
\renewcommand*\env@matrix[1][r]{\hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{*\c@MaxMatrixCols #1}}
\makeatother


%--- HEADER ----------------------------

\usepackage{fancyhdr}
\pagestyle{fancy}
\setlength{\headheight}{24pt}
\fancyhf{}
\lhead{MC920 -- Introdução ao Processamento de Imagem Digital}
\rhead{\today}

%----------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------

\begin{document}

\begin{center}
    {\LARGE Trabalho 5 - Transformações Geométricas com Interpolação} \\[10pt]
    {\large Leonardo de Sousa Rodrigues -- RA: 220063}
\end{center}

\section{Introdução}

    Esse trabalho tem como objetivo o estudo de transformações geométricas básicas em imagens digitais -- em específico, operações de rotação e mudança de escala. Embora sejam simples, tais operações têm alguns detalhes interessantes de implementação, que serão analisados abaixo.

    Essas transformações são dadas por matrizes que devem ser aplicadas às coordenadas dos pontos da imagem para obter a nova posição. Essa, no entanto, não é necessariamente um valor inteiro e, por isso, faz-se necessário o uso de algum método de interpolação para determinar o novo valor do píxel para a imagem de saída.

\section{Execução}

    O programa \texttt{transforma.py} realiza todas as etapas das transformações. Ele recebe 2 argumentos posicionais e alguns opcionais, como mostra a tabela~\ref{tab:args}.

    \begin{table}[H]
    \def\arraystretch{1.5}\tabcolsep=15pt
    \centering
    \begin{tabular}{l p{0.7\textwidth}}
        \toprule
        \multicolumn{1}{c}{\textbf{Argumento}} & \multicolumn{1}{c}{\textbf{Descrição}} \\
        \midrule
        \multicolumn{1}{c}{IMAGEM} & Arquivo de imagem a ser utilizado. \\
        \multicolumn{1}{c}{SAIDA} & Nome do arquivo em que a imagem de saída será salva. \\
        \texttt{-a, --angulo} & Recebe um valor de ângulo em graus para realizar a rotação. \\
        \texttt{-e, --escala} & Recebe um fator de escala para os dois eixos da imagem. \\
        \texttt{-d, --dimensao} & Recebe dois valores (largura e altura) para a dimensão final da imagem. \\
        \texttt{-m, --metodo} & Recebe o método de interpolação a ser utilizado, dentre os da tabela~\ref{tab:metodos} \\
        \bottomrule
    \end{tabular}
    \caption{Argumentos do programa \texttt{transforma.py} e suas descrições.}
    \label{tab:args}
    \end{table}

    \begin{table}[H]
        \def\arraystretch{1.5}\tabcolsep=15pt
        \centering
        \begin{tabular}{l l}
            \toprule
            \multicolumn{1}{c}{\textbf{Chave}} & \multicolumn{1}{c}{\textbf{Método}} \\
            \midrule
            \texttt{vizinho} & Vizinho mais próximo \\
            \texttt{bilinear} & Interpolação bilinear \\
            \texttt{bicubica} & Interpolação bicúbica \\
            \texttt{lagrange} & Polinômios de Lagrange \\
            \bottomrule
        \end{tabular}
        \caption{Métodos de interpolação disponíveis e suas chaves para utilização com \texttt{--metodo}.}
        \label{tab:metodos}
        \end{table}

    Exemplo de utilização:

    \vspace{5pt}
    \hspace{\parindent} \texttt{python transforma.py entrada/city.png saida/city.png -a 30 -e 1.5 -m lagrange}
    \vspace{5pt}

    Nesse caso, a imagem \texttt{entrada/city.png} será rotacionada $30^\circ$ no sentido antihorário e então será feita a interpolação pelo método dos polinômios de Lagrange. Depois disso, o resultado será redimensionado com um fator de escala 1.5 e haverá uma nova interpolação. A imagem final será salva em \texttt{saida/city.png}.


\section{Implementação}

    As imagens utilizadas são lidas e escritas utilizando o \texttt{OpenCV}. Todas as imagens lidas são convertidas para o formato BGRA, de forma que o fundo da imagem no caso de transformações de rotação possam ser transparentes.

    \subsection{Transformações Geométricas}

        A classe \texttt{Imagem} possui dois atributos: \texttt{matriz} e \texttt{lista}. O primeiro trata-se do array que representa a imagem, cujo formato é $(h, w, 4)$, em que $h$ é a altura da imagem, $w$ é a largura e o valor de 4 é devido ao formato BGRA.

        O atributo \texttt{lista} é uma matriz que contém as coordenadas de todos os pontos da matriz. Eles são armazenados no formato $(x, y, 1)$, que são as colunas da matriz, de forma que o formato é $(3, h \cdot w)$. Esse atributo é utilizado para aplicar as transformações aos pontos: é possível mostrar que aplicar uma transformação $T$ em diversos pontos $P_i$ é equivalente a aplicar a mesma transformação em toda a matriz:

        \begin{equation*}
            \begin{bmatrix}
                T P_1 & T P_2 & \cdots & T P_n
            \end{bmatrix} = 
            T \begin{bmatrix}
                P_1 & P_2 & \cdots & P_n
            \end{bmatrix}
        \end{equation*}

        A classe \texttt{Imagem} também possui dois métodos: \texttt{rotaciona} e \texttt{reescala}, cujas matrizes de transformação são mostradas na equação~\eqref{transf}.

        \begin{equation} \label{transf}
            T_{_{ROT}}(\theta) = \begin{bmatrix}
                  \cos{\theta} & \sin{\theta} & 0 \\
                - \sin{\theta} & \cos{\theta} & 0 \\
                0 & 0 & 1 \\
            \end{bmatrix}, \quad
            T_{_{ESC}}(s_x, s_y) = \begin{bmatrix}
                s_x & 0 & 0 \\
                0 & s_y & 0 \\
                0 & 0 & 1 \\
            \end{bmatrix}
        \end{equation}

        Em um primeiro momento, a transformação é aplicada aos 4 pontos extremos da imagem, para que seja possível encontrar a posição e o tamanho da imagem transformada. Depois disso, é criada uma imagem de saída com valor zero nos píxels. A transformação inversa $T^{-1}$ é então aplicada à nova imagem, com os ajustes de posicionamento necessários. Com os valores $x'$ e $y'$ resultantes, é feita a interpolação para encontrar o valor a ser colocado no píxel.

    \subsection{Métodos de Interpolação}

        Cada método de interpolação têm uma função, implementada de acordo com o especificado no enunciado do trabalho. Todas as operações são realizadas de forma vetorizada, utilizando arrays de valores para $x$, $y$, $dx$ e $dy$.

\newpage
\section{Resultados}

    \subsection{Vizinho Mais Próximo}

        O método do vizinho mais próximo é o mais simples dos aqui apresentados. Ele consiste em simplesmente utilizar o valor correspondente ao píxel mais próximo. Por ser simples, o método tem tempo de execução muito curto e o resultado é bom para imagens grandes. Para imagens muito pequenas, no entanto, o método deixa a desejar se comparado com os outros.

        \begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{../projeto/entrada/mario.png}
            \caption{Original.}
        \end{subfigure}
        \begin{subfigure}[b]{0.55\textwidth}
            \centering
            \includegraphics[width=\textwidth]{../projeto/saida/vizinho/mario.png}
            \caption{Rotacionada.}
        \end{subfigure}
        \caption{Figura original e rotacionada utilizando o método do vizinho mais próximo.}
        \end{figure}

        \begin{figure}[H]
            \centering
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/entrada/city.png}
                \caption{Original.}
            \end{subfigure}
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/saida/vizinho/city.png}
                \caption{Reduzida para dimensão $100 \times 100$.}
            \end{subfigure}
            \caption{Figura original e reduzida utilizando o método do vizinho mais próximo.}
        \end{figure}

    \newpage
    \subsection{Interpolação Bilinear}

        O método de interpolação bilinear considera os vizinhos mais próximos do ponto nos dois sentidos e faz uma média ponderada entre os valores, assumindo assim linearidade. Como o cálculo a ser feito é simples, o tempo de execução ainda é baixo, embora maior do que para o vizinho mais próximo.

        O resultado do método difere do vizinho mais próximo por pouco, já que ameniza levemente as altas frequências da imagem ao fazer a interpolação, assim como os métodos a seguir.

        \begin{figure}[H]
            \centering
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/entrada/galinha.png}
                \caption{Original.}
            \end{subfigure}
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/saida/bilinear/galinha.png}
                \caption{Rotacionada e reduzida por fator 0.3.}
            \end{subfigure}
            \caption{Figura original e transformada utilizando o método da interpolação bilinear.}
        \end{figure}

        \begin{figure}[H]
            \centering
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/entrada/seagull.png}
                \caption{Original.}
            \end{subfigure}
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/saida/bilinear/seagull.png}
                \caption{Reduzida para dimensões $300 \times 200$}
            \end{subfigure}
            \caption{Figura original e reduzida utilizando o método da interpolação bilinear.}
        \end{figure}

    \newpage
    \subsection{Interpolação Bicúbica}

        A interpolação bicúbica é o método que exige mais tempo de execução devido à complexidade dos cálculos realizados. Para imagens grandes esse tempo pode chegar a 90 s. Além disso, o resultado é extremamente semelhante com a interpolação bilinear, que é muito mais rápida.

        \begin{figure}[H]
            \centering
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/entrada/house.png}
                \caption{Original.}
            \end{subfigure}
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/saida/bicubica/house.png}
                \caption{Rotacionada.}
            \end{subfigure}
            \caption{Figura original e rotacionada utilizando o método da interpolação bicúbica.}
        \end{figure}

        \begin{figure}[H]
            \centering
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/entrada/dados.png}
                \caption{Original.}
            \end{subfigure}
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/saida/bicubica/dados.png}
                \caption{Rotacionada e ampliada para $1000 \times 1000$.}
            \end{subfigure}
            \caption{Figura original e transformada utilizando o método da interpolação bicúbica.}
        \end{figure}

    \newpage
    \subsection{Polinômios de Lagrange}

        O método dos polinômios de Lagrange, embora pareça mais complexo, é mais rápido de ser executado em relação à interpolação bicúbica se implementado de forma vetorizada. Os resultados também são extremamtente parecidos.

        \begin{figure}[H]
            \centering
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/entrada/baboon.png}
                \caption{Original.}
            \end{subfigure}
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{../projeto/saida/lagrange/baboon.png}
                \caption{Rotacionada e reduzida por fator 0.9.}
            \end{subfigure}
            \caption{Figura original e transformada utilizando o método dos polinômios de Lagrange.}
        \end{figure}

        \begin{figure}[H]
            \centering
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[height=\textwidth]{../projeto/entrada/butterfly.png}
                \caption{Original.}
            \end{subfigure}
            \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[height=\textwidth]{../projeto/saida/lagrange/butterfly.png}
                \caption{Mudança de escala para $500 \times 800$.}
            \end{subfigure}
            \caption{Figura original e transformada utilizando o método dos polinômios de Lagrange.}
        \end{figure}
    
\newpage
\section{Conclusão}

    A partir dos resultados mostrados acima, é possível concluir que a rotação e a mudança de escala funcionaram corretamente nas imagens, já que as imagens ficaram como esperado e têm as dimensões esperadas.

    A principal diferença entre os métodos de interpolação foi o tempo de execução do programa, com destaque para a rapidez do método do vizinho mais próximo e para a demora da interpolação bicúbica. Nesse ponto, é válido ressaltar que a implemetação vetorizada foi de grande importância para a execução em um tempo viável de todos os métodos.

    Por fim, uma mudança que poderia ser feita para otimizar ainda mais o programa é a realização de apenas uma interpolação depois da rotação e da mudança de escala. O modo feito faz com que depois de cada uma das duas etapas ocorra uma interpolação.   

\end{document}
