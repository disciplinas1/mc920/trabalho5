"""
Realiza transformações de rotação e
escala em imagens coloridas.
"""
import numpy as np
import cv2
import argparse


class Imagem:

    def __init__(self, matriz: np.ndarray):
        self.matriz = matriz
        self.lista  = self.matriz_para_lista()


    def matriz_para_lista(self) -> np.ndarray:
        """Cria uma lista de pontos [x, y, 1] da imagem a partir da matriz."""
        h, w = self.matriz.shape[:2]

        # Cria a matriz no formato (3, w.h).
        lista = np.ones((3, w * h))

        # Coloca o valor de x e y para cada ponto da imagem na lista.
        x = np.arange(w)
        y = np.arange(h)

        lista[0] = np.tile(x, h)
        lista[1] = np.repeat(y, w)

        return lista


    def rotaciona(self, angulo: float, metodo: str='vizinho') -> 'Imagem':
        """
        Rotaciona a imagem por um ângulo em graus,
        utilizando o método de interpolação dado.
        """
        rad = angulo * np.pi / 180

        cos = np.cos(rad)
        sin = np.sin(rad)

        # Calcula T e a inversa de T.
        t = np.asarray([
            [ cos, sin, 0],
            [-sin, cos, 0],
            [   0,   0, 1]
        ])

        t_inv = np.transpose(t)

        h, w = self.matriz.shape[:2]

        # Acha os pontos extremos do retângulo e aplica a transformação T
        # para achar o shape da imagem de saída.
        ext = np.asarray([
            [0, 0, w, w],
            [0, h, 0, h],
            [1, 1, 1, 1]
        ])

        ext_transf = t @ ext

        xmin = int(np.min(ext_transf[0]))
        xmax = int(np.max(ext_transf[0]))
        ymin = int(np.min(ext_transf[1]))
        ymax = int(np.max(ext_transf[1]))

        # Cria a nova imagem com shape adequado.
        nova = Imagem(np.zeros((ymax - ymin, xmax - xmin, self.matriz.shape[2])))

        # Shift a ser aplicado nos pontos.
        shift = np.asarray([
            [1, 0, xmin],
            [0, 1, ymin],
            [0, 0, 1]
        ])

        globals()[metodo](self, nova, t_inv, shift)

        return nova


    def reescala(self, x_fator: float, y_fator: float, metodo: str='vizinho') -> 'Imagem':
        """
        Reescala a imagem por fatores diferentes em x e y,
        utilizando o método de interpolação dado.
        """
        # Calcula T e a inversa de T.
        t = np.asarray([
            [x_fator, 0, 0],
            [0, y_fator, 0],
            [0,       0, 1]
        ])

        t_inv = np.asarray([
            [1 / x_fator, 0, 0],
            [0, 1 / y_fator, 0],
            [0,           0, 1]
        ])

        h, w = self.matriz.shape[:2]

        # Acha os pontos extremos do retângulo e aplica a transformação T
        # para achar o shape da imagem de saída.
        ext = np.asarray([
            [0, 0, w, w],
            [0, h, 0, h],
            [1, 1, 1, 1]
        ])

        ext_transf = t @ ext

        xmin = int(np.min(ext_transf[0]))
        xmax = int(np.max(ext_transf[0]))
        ymin = int(np.min(ext_transf[1]))
        ymax = int(np.max(ext_transf[1]))

        # Cria a nova imagem com shape adequado.
        nova = Imagem(np.zeros((ymax - ymin, xmax - xmin, self.matriz.shape[2])))

        # Shift nulo para operação de escala.
        shift = np.identity(3)

        globals()[metodo](self, nova, t_inv, shift)

        return nova


def vizinho(original: Imagem, nova: Imagem, t_inv: np.ndarray, shift: np.ndarray) -> None:
    """
    Preenche a imagem nova com os valores adequados, utilizando
    a matriz da transformação inversa e a imagem original.
    Interpola pelo método do vizinho mais próximo.
    """
    h, w = original.matriz.shape[:2]

    # Aplica a transformação inversa na lista de pontos da nova imagem.
    nova_t_inv = t_inv @ shift @ nova.lista

    # Lista de valores de x e y para aplicação da interpolação.
    xl = nova_t_inv[0]
    yl = nova_t_inv[1]

    x = np.round(xl).astype(int)
    y = np.round(yl).astype(int)

    # Lista booleana do tamanho de x e y, com True nos elementos dentro da imagem.
    dentro = (x >= 0) & (x < w) & (y >= 0) & (y < h)

    # Filtra os valores de x e y dentro da imagem.
    x = x[dentro]
    y = y[dentro]

    # Transforma a lista em uma matriz para indexar a imagem de saída.
    dentro = dentro.reshape(nova.matriz.shape[:2])
    
    nova.matriz[dentro] = original.matriz[y, x]


def bilinear(original: Imagem, nova: Imagem, t_inv: np.ndarray, shift: np.ndarray) -> None:
    """
    Preenche a imagem nova com os valores adequados, utilizando
    a matriz da transformação inversa e a imagem original.
    Interpola pelo método bilinear.
    """
    h, w = original.matriz.shape[:2]

    # Aplica a transformação inversa na lista de pontos da nova imagem.
    nova_t_inv = t_inv @ shift @ nova.lista

    # Lista de valores de x e y para aplicação da interpolação.
    xl = nova_t_inv[0]
    yl = nova_t_inv[1]

    xt = np.floor(xl).astype(int)
    yt = np.floor(yl).astype(int)

    dx = (xl - xt)
    dy = (yl - yt)

    # Lista booleana do tamanho de x e y, com True nos elementos dentro da imagem.
    dentro = (xt >= 1) & (xt < w - 1) & (yt >= 1) & (yt < h - 1)

    xt = xt[dentro]
    yt = yt[dentro]

    dx = dx[dentro, np.newaxis]
    dy = dy[dentro, np.newaxis]

    dentro = dentro.reshape(nova.matriz.shape[:2])

    f = original.matriz

    a = (1 - dx) * (1 - dy) * f[yt, xt] + dx * (1 - dy) * f[yt, xt + 1] + (1 - dx) * dy * f[yt + 1, xt] + dx * dy * f[yt + 1, xt + 1]

    nova.matriz[dentro] = a.astype(np.uint8)


def bicubica(original: Imagem, nova: Imagem, t_inv: np.ndarray, shift: np.ndarray) -> None:
    """
    Preenche a imagem nova com os valores adequados, utilizando
    a matriz da transformação inversa e a imagem original.
    Interpola pelo método bicúbico.
    """
    def P(t: np.ndarray) -> np.ndarray:
        return np.where(t > 0, t, 0)

    def R(s):
        return (1 / 6) * (P(s + 2)**3 - 4 * P(s + 1)**3 + 6 * P(s)**3 - 4 * P(s - 1)**3)

    h, w = original.matriz.shape[:2]

    # Aplica a transformação inversa na lista de pontos da nova imagem.
    nova_t_inv = t_inv @ shift @ nova.lista

    # Lista de valores de x e y para aplicação da interpolação.
    xl = nova_t_inv[0]
    yl = nova_t_inv[1]

    xt = np.floor(xl).astype(int)
    yt = np.floor(yl).astype(int)

    dx = (xl - xt)
    dy = (yl - yt)

    # Lista booleana do tamanho de x e y, com True nos elementos dentro da imagem.
    dentro = (xt >= 2) & (xt < w - 2) & (yt >= 2) & (yt < h - 2)

    xt = xt[dentro]
    yt = yt[dentro]

    dx = dx[dentro, np.newaxis]
    dy = dy[dentro, np.newaxis]

    dentro = dentro.reshape(nova.matriz.shape[:2])

    f = original.matriz

    for m in range(-1, 2 + 1):
        for n in range(-1, 2 + 1):

            nova.matriz[dentro] += f[yt + n, xt + m] * R(m - dx) * R(dy - n)


def lagrange(original: Imagem, nova: Imagem, t_inv: np.ndarray, shift: np.ndarray) -> None:
    """
    Preenche a imagem nova com os valores adequados, utilizando
    a matriz da transformação inversa e a imagem original.
    Interpola pelo método dos polinômios de Lagrange.
    """
    def L(n: int, xt: np.ndarray, yt: np.ndarray, dx: np.ndarray, dy: np.ndarray, f) -> np.ndarray:
        ret  = (1 / 6) * (-dx * (dx - 1) * (dx - 2) * f[yt + n - 2, xt - 1])
        ret += (1 / 2) * ((dx + 1) * (dx - 1) * (dx - 2) * f[yt + n - 2, xt])
        ret += (1 / 2) * (-dx * (dx + 1) * (dx - 2) * f[yt + n - 2, xt + 1])
        ret += (1 / 6) * (dx * (dx + 1) * (dx - 1) * f[yt + n - 2, xt + 2])

        return ret

    h, w = original.matriz.shape[:2]

    # Aplica a transformação inversa na lista de pontos da nova imagem.
    nova_t_inv = t_inv @ shift @ nova.lista

    # Lista de valores de x e y para aplicação da interpolação.
    xl = nova_t_inv[0]
    yl = nova_t_inv[1]

    xt = np.floor(xl).astype(int)
    yt = np.floor(yl).astype(int)

    dx = (xl - xt)
    dy = (yl - yt)

    # Lista booleana do tamanho de x e y, com True nos elementos dentro da imagem.
    dentro = (xt >= 2) & (xt < w - 2) & (yt >= 2) & (yt < h - 2)

    xt = xt[dentro]
    yt = yt[dentro]

    dx = dx[dentro, np.newaxis]
    dy = dy[dentro, np.newaxis]

    dentro = dentro.reshape(nova.matriz.shape[:2])

    f = original.matriz

    nova.matriz[dentro]  = (1 / 6) * (-dy * (dy - 1) * (dy - 2) * L(1, xt, yt, dx, dy, f))
    nova.matriz[dentro] += (1 / 2) * ((dy + 1) * (dy - 1) * (dy - 2) * L(2, xt, yt, dx, dy, f))
    nova.matriz[dentro] += (1 / 2) * (-dy * (dy + 1) * (dy - 2) * L(3, xt, yt, dx, dy, f))
    nova.matriz[dentro] += (1 / 6) * (dy * (dy + 1) * (dy - 1) * L(4, xt, yt, dx, dy, f))


def main():
    parser = argparse.ArgumentParser(description='Realiza transformações em imagens coloridas.')

    parser.add_argument('IMAGEM', action='store', nargs=1, type=str,
        help='arquivo da imagem que será utilizada'
    )

    parser.add_argument('SAIDA', action='store', nargs=1, type=str,
        help='arquivo para salvar imagem de saída'
    )

    escala = parser.add_mutually_exclusive_group()

    parser.add_argument('-a', '--angulo', action='store', nargs=1, type=float,
        help='ângulo para realizar a rotação da imagem'
    )

    escala.add_argument('-e', '--escala', action='store', nargs=1, type=float,
        help='fator de escala a ser aplicado'
    )

    escala.add_argument('-d', '--dimensao', action='store', nargs=2, type=int,
        help='dimensão da imagem final a ser salva'
    )

    parser.add_argument('-m', '--metodo', action='store', nargs=1, type=str, default=['vizinho'],
        help='método de interpolação a ser utilizado'
    )

    args = parser.parse_args()

    # Leitura da imagem.
    entrada = Imagem(cv2.imread(args.IMAGEM[0], flags=cv2.IMREAD_UNCHANGED))

    # Force image to be in BGRA format.p
    if entrada.matriz.ndim == 2:
        entrada.matriz = cv2.cvtColor(entrada.matriz, cv2.COLOR_GRAY2BGRA)
    elif entrada.matriz.shape[2] == 3:
        entrada.matriz = cv2.cvtColor(entrada.matriz, cv2.COLOR_BGR2BGRA)


    # Aplica a rotação.
    if args.angulo is not None:
        rot = entrada.rotaciona(args.angulo[0], args.metodo[0])
    else:
        rot = entrada


    # Aplica a escala
    if args.escala is not None:
        esc = rot.reescala(args.escala[0], args.escala[0], args.metodo[0])

    elif args.dimensao is not None:
        h, w = rot.matriz.shape[:2]
        x_fator = args.dimensao[0] / w
        y_fator = args.dimensao[1] / h
        
        esc = rot.reescala(x_fator, y_fator, args.metodo[0])

    else:
        esc = rot


    cv2.imwrite(args.SAIDA[0], esc.matriz)


if __name__ == '__main__':
    main()
